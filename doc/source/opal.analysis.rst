opal.analysis package
=====================

Notebooks
---------

.. toctree::
   cyclotron<Cyclotron>

opal.analysis.FieldAnalysis module
---------------------------------

.. automodule:: opal.analysis.FieldAnalysis

opal.analysis.H5Statistics module
---------------------------------

.. automodule:: opal.analysis.H5Statistics

opal.analysis.OptimizerAnalysis module
--------------------------------------

.. automodule:: opal.analysis.OptimizerAnalysis

opal.analysis.SamplerStatistics module
--------------------------------------

.. automodule:: opal.analysis.SamplerStatistics

opal.analysis.Statistics module
-------------------------------

.. automodule:: opal.analysis.Statistics

opal.analysis.StdOpalOutputAnalysis module
------------------------------------------

.. automodule:: opal.analysis.StdOpalOutputAnalysis

opal.analysis.TrackOrbitAnalysis module
---------------------------------------

.. automodule:: opal.analysis.TrackOrbitAnalysis

opal.analysis.cyclotron module
------------------------------

.. automodule:: opal.analysis.cyclotron

opal.analysis.pareto\_fronts module
-----------------------------------

.. automodule:: opal.analysis.pareto_fronts