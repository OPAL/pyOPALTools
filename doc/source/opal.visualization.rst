opal.visualization package
==========================

Subpackages
-----------

.. toctree::

   opal.visualization.statistics
   opal.visualization.styles

Notebooks
---------

.. toctree::

   AmrPlotter<testAmrOpal-1.py>
   FieldPlotter<FieldDataset>
   H5Plotter_RingMultiBunch<RingMultiBunch>
   ProbePlotter<Probe>
   ProbePlotter_PeakFile<ProbePeak>
   OptimizerPlotter<Optimizer>
   SamplerPlotter<Sampler>
   StatPlotter_FODO<FODO>
   StatPlotter_Gantry<Gantry2>
   StatPlotter_RingCyclotron<RingCyclotron>

opal.visualization.AmrPlotter module
------------------------------------

.. automodule:: opal.visualization.AmrPlotter

opal.visualization.BasePlotter module
-------------------------------------

.. automodule:: opal.visualization.BasePlotter

opal.visualization.FieldPlotter module
-------------------------------------

.. automodule:: opal.visualization.FieldPlotter

opal.visualization.GridPlotter module
-------------------------------------

.. automodule:: opal.visualization.GridPlotter

opal.visualization.H5Plotter module
-----------------------------------

.. automodule:: opal.visualization.H5Plotter

opal.visualization.MemoryPlotter module
---------------------------------------

.. automodule:: opal.visualization.MemoryPlotter

opal.visualization.OptimizerPlotter module
------------------------------------------

.. automodule:: opal.visualization.OptimizerPlotter

opal.visualization.PeakPlotter module
-------------------------------------

.. automodule:: opal.visualization.PeakPlotter

opal.visualization.ProbePlotter module
--------------------------------------

.. automodule:: opal.visualization.ProbePlotter

opal.visualization.ProfilingPlotter module
------------------------------------------

.. automodule:: opal.visualization.ProfilingPlotter

opal.visualization.SamplerPlotter module
----------------------------------------

.. automodule:: opal.visualization.SamplerPlotter

opal.visualization.SolverPlotter module
---------------------------------------

.. automodule:: opal.visualization.SolverPlotter

opal.visualization.StatPlotter module
-------------------------------------

.. automodule:: opal.visualization.StatPlotter

opal.visualization.StdOpalOutputPlotter module
----------------------------------------------

.. automodule:: opal.visualization.StdOpalOutputPlotter

opal.visualization.TimingPlotter module
---------------------------------------

.. automodule:: opal.visualization.TimingPlotter

opal.visualization.TrackOrbitPlotter module
-------------------------------------------

.. automodule:: opal.visualization.TrackOrbitPlotter

opal.visualization.formatter module
-----------------------------------

.. automodule:: opal.visualization.formatter