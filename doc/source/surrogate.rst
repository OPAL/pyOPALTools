surrogate package
=================

Notebooks
---------

.. toctree::

   uq_example<uq_example>

surrogate.bootstrap.bootstrap module
------------------------------------

.. automodule:: surrogate.bootstrap.bootstrap

surrogate.chaospy\_model module
-------------------------------

.. automodule:: surrogate.chaospy\_model

surrogate.uqtk\_model module
----------------------------

.. automodule:: surrogate.uqtk\_model