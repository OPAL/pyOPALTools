# Copyright (c) 2020, Paul Scherrer Institut, Villigen PSI, Switzerland
# All rights reserved
#
# This file is part of pyOPALTools.
#
# pyOPALTools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# You should have received a copy of the GNU General Public License
# along with pyOPALTools. If not, see <https://www.gnu.org/licenses/>.

class BaseParser:
    def check_file(self, filename):
        """Check if appropriate file.

        Parameters
        ----------
        filename : str
           file to be checked

        Returns
        -------
        bool
            True if a valid file, otherwise False.
        """
        try:
            self.parse(filename)
            self.clear()
        except:
            self.clear()
            return False
        return True